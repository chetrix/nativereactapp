import React, {useEffect, useState, useRef} from 'react';
import io from "socket.io-client";
import { GiftedChat } from 'react-native-gifted-chat'
export default function HomeScreen(){
    const socket = useRef(null);
    const [recvMessages, setRecvMessages] = useState([]);
  useEffect(()=>{
    socket.current = io("http://192.168.100.19:3001");
    socket.current.on("message", message => {
      alert(1);
      setRecvMessages(prevState=> GiftedChat.append(prevState, message));
    })
  },[])
  const onSend = (messages) => {
      socket.current.emit("message", messages[0].text);
      setRecvMessages(prevState => GiftedChat.append(prevState, messages ))
      console.log(messages);
  }
  console.log(recvMessages);
  return (
      <GiftedChat
      messages={recvMessages}
      onSend={messages => onSend(messages)}
      user={{
        _id: 1,
      }}
    />
  );
};